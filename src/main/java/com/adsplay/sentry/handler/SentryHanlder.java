package com.adsplay.sentry.handler;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import io.sentry.Sentry;
import io.sentry.SentryClient;
import io.sentry.SentryClientFactory;
import io.sentry.context.Context;
import io.sentry.event.BreadcrumbBuilder;
import io.sentry.event.UserBuilder;
import rfx.core.util.FileUtils;

public class SentryHanlder {

    private static final int PERIOD_TIME = 30 * 1000;

    private static final String EMPTY = "";

    private static SentryClient sentryClient;

    private static final String CONFIG_PATH = "/" + "configs/" + "sentry.properties";

    private static Properties properties = new Properties();

    private static Timer timer = new Timer();

    private static int count = 0;
    static {
        init();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                init();
            }
        }, 1, PERIOD_TIME);
    }

    private static void init() {
        DataInputStream streamConfigFile = FileUtils.readFileAsStream(CONFIG_PATH);
        try {
            if (count == 0) {
                properties.load(streamConfigFile);
                count++;
            } else {
                Properties newProperties = new Properties();

                newProperties.load(streamConfigFile);
                if (!newProperties.equals(properties)) {
                    properties = newProperties;
                    System.out.println("Properties changed!!!");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(properties.toString());
        String dsn = properties.getProperty("sentry.dsn");
        Sentry.init(dsn);
        sentryClient = SentryClientFactory.sentryClient(dsn);
    }

    public static void main(String... args) {
        SentryHanlder.sendErrorToSentryLog("action8", "tnhminh1@fpt.com", null, null, "Capture18", new NumberFormatException(), true);
       // logWithStaticAPI();
        
    }

    /**
     * Examples using the (recommended) static API.
     */
    public static void sendErrorToSentryLog(String action, String emailAddr, Map<String, Object> extraDatas, Map<String, Object> tags,
            String message, Exception exception, boolean isStaticMethod) {

        if (!"true".equals(properties.getProperty("sentry.turn.on"))) {
            return;
        }

        Context context = isStaticMethod ? Sentry.getContext() : sentryClient.getContext();

        context.recordBreadcrumb(new BreadcrumbBuilder().setMessage(message != null ? action + "|" + message : "no action").build());

        // Set the user in the current context.
        context.setUser(new UserBuilder().setEmail((emailAddr) != null ? emailAddr : EMPTY).build());

        // Add extra data to future events in this context.
        if (extraDatas != null) {
            for (Entry<String, Object> data : extraDatas.entrySet()) {
                context.addExtra(data.getKey(), data.getValue().toString());
            }
        }

        // Add an additional tag to future events in this context.
        if (tags != null) {
            for (Entry<String, Object> tag : tags.entrySet()) {
                context.addTag(tag.getKey(), tag.getValue().toString());
            }
        }

        if (isStaticMethod) {
            Sentry.capture(message);
            Sentry.capture(exception);
        } else {
            sentryClient.sendMessage(message);
            sentryClient.sendException(exception);
        }
    }
}