package com.adsplay.sentry.handler;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Test {
    public static void main(String[] args) throws Exception {
        ExecutorService executor = Executors.newFixedThreadPool(8);
        for (int i = 0; i <= 100; i++) {
            Future<String> future = executor.submit(new Task("Task" + i));
            String x = "";
            try {
                x = future.get(1, TimeUnit.MILLISECONDS);
                System.out.println("Started..");
                System.out.println(x);
                System.out.println("Finished!");
            } catch (TimeoutException e) {
                future.cancel(true);
                System.out.println("Terminated!-" + x);
            }

        }

        executor.shutdownNow();
    }
}

class Task implements Callable<String> {
    private String name;

    public Task(String taskName) {
        this.name = taskName;
    }

    @Override
    public String call() throws Exception {
        Thread.sleep(200); // Just to demo a long running task of 4 seconds.
        return "Ready-" + name;
    }
}